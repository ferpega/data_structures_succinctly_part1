﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MisAlgoritmos
{
    public class MyLinkedList<T> : ICollection<T>
    {
        private MyLinkedListNode<T> _first;
        private MyLinkedListNode<T> _last;

        public int Count { get; private set; } = 0;

        public bool IsReadOnly
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public void Add(T item)
        {
            if (NoItemsInList())
            {
                AddFirstItem(item);
            }
            else
            {
                AddNextItem(item);
            }

        }

        private bool NoItemsInList()
        {
            return _first == null;
        }

        private void AddFirstItem(T item)
        {
            _first = new MyLinkedListNode<T>(item);
            _last = _first;
            Count = 1;
        }
        private void AddNextItem(T item)
        {
            var previousLast = _last;

            _last = new MyLinkedListNode<T>(item);
            previousLast.NextNode = _last;
            Count++;
        }

        public MyLinkedListNode<T> First { get { return _first; } }
        public MyLinkedListNode<T> Last { get { return _last; } }

        public void Clear()
        {
            throw new NotImplementedException();
        }

        public bool Contains(T item)
        {
            throw new NotImplementedException();
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }

        public IEnumerator<T> GetEnumerator()
        {
            throw new NotImplementedException();
        }

        public bool Remove(T item)
        {
            throw new NotImplementedException();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }
    }

    public class MyLinkedListNode<T>
    {
        public MyLinkedListNode(T value)
        {
            Value = value;
        }

        public T Value { get; private set; }
        public MyLinkedListNode<T> NextNode { get; set; }
    }
}
