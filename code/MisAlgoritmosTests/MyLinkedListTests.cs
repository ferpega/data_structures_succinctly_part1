﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MisAlgoritmos;

namespace MisAlgoritmosTests
{
    [TestClass]
    public class MyLinkedListTests
    {

        [TestMethod]
        public void InitialStatus()
        {
            var sut = new MyLinkedList<int>();

            Assert.IsNull(sut.First);
            Assert.IsNull(sut.Last);
            Assert.AreEqual(0, sut.Count);
        }

        [TestMethod]
        public void Add_First()
        {
            var sut = new MyLinkedList<int>();
            sut.Add(5);
            
            var result = sut.First.Value;

            Assert.AreEqual(1, sut.Count);
            Assert.AreEqual(5, result);
        }

        [TestMethod]
        public void Add_Two()
        {
            var sut = new MyLinkedList<int>();
            sut.Add(5);
            sut.Add(6);
           
            Assert.AreEqual(5, sut.First.Value);
            Assert.AreEqual(6, sut.Last.Value);
            Assert.AreEqual(2, sut.Count);
        }


        [TestMethod]
        public void Add_Three()
        {
            var sut = new MyLinkedList<int>();
            sut.Add(5);
            sut.Add(6);
            sut.Add(1);

            Assert.AreEqual(5, sut.First.Value);
            Assert.AreEqual(1, sut.Last.Value);
            Assert.AreEqual(3, sut.Count);
        }
    }
}
